import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';

import { TasksServiceProvider } from '../../providers/tasks-service/tasks-service';
import { UsersServiceProvider } from '../../providers/users-service/users-service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {
  ngOnInit(): void {
    this.cargarUsuarios();
  }

  usuarios: any[];
  nomUsuario: string[] = [];

  // public properties

  tasks: any[] = [];

  // private fields 

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public tasksService: TasksServiceProvider,
    public http: TasksServiceProvider,
    public myToastCtrl: ToastController,
    public httpj: HttpClient
  ) { }

  // Ionic's or Angular's triggers

  ionViewDidEnter() {
    this.getAllTasks();

  }

  // public methods

  deleteTask(task: any, index) {
    this.tasksService.delete(task)
      .then(response => {
        console.log(response);
        this.tasks.splice(index, 1);
      })
      .catch(error => {
        console.error(error);
      })
  }

  getAllTasks() {
    this.tasksService.getAll()
      .then(tasks => {
        console.log(tasks);
        this.tasks = tasks;
      })
      .catch(error => {
        console.error(error);
      })
  }

  openAlertNewTask() {
    let alert = this.alertCtrl.create({
      title: 'Adición de Usuarios',
      message: 'Escriba el nombre del usuario',
      inputs: [
        {
          name: 'title',
          placeholder: 'Digite aquí el nombre.',
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('cancelar');
          }
        },
        {
          text: 'Guardar',
          handler: (data) => {
            data.completed = false;
            this.tasksService.create(data)
              .then(response => {
                this.tasks.unshift(data);
              })
              .catch(error => {
                console.error(error);
              })
          }
        }
      ]
    });
    alert.present();
  }
  onChange(task: any) {
    task.completed = !task.completed;
    this.tasksService.update(task);
  }

  updateTask(task: any, index: any) {
    let prompt = this.alertCtrl.create({
      title: 'Actualizar Usuario',
      message: "Digite el nombre del usuario",
      inputs: [
        {
          name: 'title',
          value: task.title,
          placeholder: 'Usuario'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Actualizar',
          handler: data => {
            let updatetask = Object.assign({}, task);
            updatetask.title = data.title;
            this.tasksService.update(updatetask)
              .then(response => {
                response = updatetask;
                this.tasks[index] = response;
                //this.tasks[index] = updatetask;
              })
              .catch(error => {
                console.error(error);
              });
          }
        }
      ]
    });
    prompt.present();
  }

  cargarUsuarios() {

    this.loadUsers().then(
      (res) => {
        this.usuarios = res['results'];
      },
      (error) => {
        console.error(error);

        let toast = this.myToastCtrl.create({
          message: 'Ups! Parece que no tienes conexión',
          duration: 3000,
          position: 'top'
        });

        toast.onDidDismiss(() => {
          console.log('toast Dismissed');
        });

        toast.present();

      }
    )
  }

  //Consultando usuarios
  loadUsers() {
    return this.httpj
      .get('https://randomuser.me/api/?results=1')
      .toPromise();

  }

  guardarUsuarios() {
    for (let i = 0; i < this.usuarios.length; i++) {
      this.nomUsuario.push(this.usuarios[i].name.first);

    }

    this.tasksService.create({title: this.nomUsuario[0]})
      .then(response => {
        this.tasks.unshift({title: this.nomUsuario[0]});
      })
      .catch(error => {
        console.error(error);
      })



  }



}
